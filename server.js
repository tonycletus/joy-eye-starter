//import app deps/libs
const express = require('express')
const app = express()

app.set('port', (process.env.PORT || 5000))

app.use(express.static(__dirname));
app.use(express.static('images'));

// HOST ROUTE (HOME PAGE)
app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html')
})

app.listen(app.get('port'), function() {
	console.log("running: port");
}) 