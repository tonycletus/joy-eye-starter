var cacheName = 'joy-eye-v6';
// var cacheFiles = [
//   './',
//   './index.html',
//   './offline.html',
//   './app.css',
//   './app.js',
//   './images/joy-eye-favicon.png',
//   './images/joy-eye-logo.png'
// ];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Installed');
});


self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activated');

  e.waitUntil(

    // Get all the cache keys (cacheName)
  caches.keys().then(function(cacheNames) {
    return Promise.all(cacheNames.map(function(thisCacheName) {

      // If a cached item is saved under a previous cacheName
      if (thisCacheName !== cacheName) {

        // Delete that cached file
        console.log('[ServiceWorker] Removing Cached Files from Cache - ', thisCacheName);
        return caches.delete(thisCacheName);
      }
    }));
  })
); // end e.waitUntil

});


self.addEventListener('fetch', event => {
  console.log('Service Worker: Fetching');
  e.respondWith(
   fetch(e.request)
   .then(res => {
     // Make copy/clone of response
     const resClone = res.clone();
     // Open cache
     caches.open(cachName).then(cache => {
       // Add response to cache
       cache.put(e.request, resClone);
     })
     return res;
   }) 
   .catch(err => caches.match(e.request). then(res => res))
  )
});